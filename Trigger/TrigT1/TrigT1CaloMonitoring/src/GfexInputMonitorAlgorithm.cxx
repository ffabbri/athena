/*
   Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
   */

#include "GfexInputMonitorAlgorithm.h"
#include "TProfile2D.h"
#include "TMath.h"
GfexInputMonitorAlgorithm::GfexInputMonitorAlgorithm( const std::string& name, ISvcLocator* pSvcLocator )
	: AthMonitorAlgorithm(name,pSvcLocator)
{
}

StatusCode GfexInputMonitorAlgorithm::initialize() {

	ATH_MSG_DEBUG("GfexInputMonitorAlgorith::initialize");
	ATH_MSG_DEBUG("Package Name "<< m_packageName);
	ATH_MSG_DEBUG("m_gFexTowerContainer"<< m_gFexTowerContainerKey);

	// we initialise all the containers that we need
	ATH_CHECK( m_gFexTowerContainerKey.initialize() );

	return AthMonitorAlgorithm::initialize();
}

StatusCode GfexInputMonitorAlgorithm::fillHistograms( const EventContext& ctx ) const {

	ATH_MSG_DEBUG("GfexInputMonitorAlgorithm::fillHistograms");

	// Access gFex gTower container
	SG::ReadHandle<xAOD::gFexTowerContainer> gFexTowerContainer{m_gFexTowerContainerKey, ctx};
	if(!gFexTowerContainer.isValid()){
		ATH_MSG_ERROR("No gFex Tower container found in storegate  "<< m_gFexTowerContainerKey);
		return StatusCode::SUCCESS;
	}

	// monitored variables for histograms
	auto nGfexTowers = Monitored::Scalar<int>("NGfexTowers",0.0);
	auto Towereta = Monitored::Scalar<float>("TowerEta",0.0);
	auto Towerphi = Monitored::Scalar<float>("TowerPhi",0.0);
	auto Towersaturationflag = Monitored::Scalar<char>("TowerSaturationflag",0.0);
	auto Toweret = Monitored::Scalar<int>("TowerEt",0);
	auto evtNumber = Monitored::Scalar<ULong64_t>("EventNumber",GetEventInfo(ctx)->eventNumber());
    auto lbnString = Monitored::Scalar<std::string>("LBNString",std::to_string(GetEventInfo(ctx)->lumiBlock()));
    auto lbn = Monitored::Scalar<int>("LBN",GetEventInfo(ctx)->lumiBlock());
	auto binNumber = Monitored::Scalar<int>("binNumber",0);
	


	unsigned int nTowers = 0;
	//ATH_MSG_INFO("gTOWER" << "    Eta " << "   Eta Index" << "  Phi " << "   Phi index " );
	for(const xAOD::gFexTower* gfexTowerRoI : *gFexTowerContainer){

		Toweret=gfexTowerRoI->towerEt();
		Towersaturationflag=gfexTowerRoI->isSaturated();
		fill("gTowers",Toweret,Towersaturationflag);

		float eta = gfexTowerRoI->eta();
		float phi = gfexTowerRoI->phi();

		Towereta = eta;
					
		if(gfexTowerRoI->towerEt() >= 200 ){
			nTowers++;
		}
		//GREATER THAN 2GEV
		if (gfexTowerRoI->towerEt() >= 10){
			if (std::abs(eta) >= 3.25 ){
				Towerphi = phi- 0.1;
				binNumber = getBinNumberTower(eta,phi-0.1,0,0);
				fill("highEtgTowers",Towereta,Towerphi);
				fill("highEtgTowers",lbn,binNumber);	
				Towerphi = phi + 0.1;
				binNumber = getBinNumberTower(eta, phi+0.1,0,0);
				fill("highEtgTowers",Towereta,Towerphi);
				fill("highEtgTowers",lbn,binNumber);
			} else {
				Towerphi = phi;
				binNumber = getBinNumberTower(eta,phi,0,0);
				fill("highEtgTowers",Towereta,Towerphi);
				fill("highEtgTowers",lbn,binNumber);
			}
			
		}
      //only for h_gTower_coldtowers_etaphimap
		else if (gfexTowerRoI->towerEt() <= -10){
			if (std::abs(eta) >= 3.25 ){
				Towerphi = phi- 0.1;
				binNumber = getBinNumberTower(eta,phi-0.1,0,0);
				fill("lowEtgTowers",Towereta,Towerphi);
				fill("lowEtgTowers",lbn,binNumber);	
				Towerphi = phi + 0.1;
				binNumber = getBinNumberTower(eta, phi+0.1,0,0);
				fill("lowEtgTowers",Towereta,Towerphi);
				fill("lowEtgTowers",lbn,binNumber);
			} else {
				Towerphi = phi;
				binNumber = getBinNumberTower(eta,phi,0,0);
				fill("lowEtgTowers",Towereta,Towerphi);
				fill("lowEtgTowers",lbn,binNumber);
			}
		}
	}

	nGfexTowers = nTowers;
	fill ("highEtgTowers",lbn,nGfexTowers);

    
	return StatusCode::SUCCESS;
}

int GfexInputMonitorAlgorithm::getBinNumberTower (const float& inputEta, const float& inputPhi, int xbin, int ybin) const{
   const std::vector<float> eta = {-4.9, -4.1,-3.5,-3.25,-3.2,-3.1,-2.9,-2.7,-2.5,-2.2,-2.0,-1.8,-1.6,-1.4,-1.2,-1.0,-0.8,-0,6,-0.4,-0.2,0.0,0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.5,2.7,2.9,3.1,3.3,3.25,3.5,4.1,4.9};

   for (int i = 0; i <= 40; i++){ 
       if (inputEta >= eta[i] && inputEta < eta[i+1]){
           xbin = i+1;
           continue;
        }
    }  
    int j=1;
	for (float phi = -3.2; phi <= 3.2;phi = phi+ 0.2){
        if (inputPhi >= phi && inputPhi < phi+0.2){
            ybin = j;
            break;
            }
        j++;
	}
    int binN = 40*(ybin-1)+xbin; 
    return binN;
}


